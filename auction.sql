-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- 主機: 127.0.0.1
-- 產生時間： 2014 年 09 月 09 日 05:21
-- 伺服器版本: 5.5.37-1
-- PHP 版本： 5.6.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 資料庫： `auction`
--
CREATE DATABASE IF NOT EXISTS `auction` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `auction`;

-- --------------------------------------------------------

--
-- 資料表結構 `item`
--

CREATE TABLE IF NOT EXISTS `item` (
`id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `detail` varchar(300) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `currentprice` int(11) NOT NULL,
  `startdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `maturity` int(11) DEFAULT '60',
  `available` tinyint(4) NOT NULL DEFAULT '1',
  `userid` int(11) NOT NULL,
  `biduserid` int(11) DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=92 ;

--
-- 資料表的匯出資料 `item`
--

INSERT INTO `item` (`id`, `title`, `detail`, `image`, `currentprice`, `startdatetime`, `maturity`, `available`, `userid`, `biduserid`) VALUES
(1, '123', '4567890', '../test/lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(2, 'testtitle', 'testdetail', 'testurl', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(3, '100', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(4, '101', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(5, '102', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(6, '103', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(7, '104', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(8, '100', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(9, '101', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(10, '102', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(11, '103', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(12, '104', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(13, '100', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(14, '101', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(15, '102', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(16, '103', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(17, '104', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(18, '100', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(19, '101', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(20, '102', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(21, '103', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(22, '104', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(23, '100', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(24, '101', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(25, '102', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(26, '103', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(27, '104', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(28, '100', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(29, '101', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(30, '102', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(31, '103', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(32, '104', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(33, '100', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(34, '101', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(35, '102', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(36, '103', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(37, '104', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(38, '100', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(39, '101', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(40, '102', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(41, '103', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(42, '104', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(43, '100', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(44, '101', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(45, '102', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(46, '103', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(47, '104', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(48, '100', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(49, '101', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(50, '102', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(51, '103', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(52, '104', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(53, '100', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(54, '101', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(55, '102', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(56, '103', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(57, '104', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(58, '100', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(59, '101', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(60, '102', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(61, '103', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(62, '104', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(63, '100', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(64, '101', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(65, '102', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(66, '103', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(67, '104', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(68, '100', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(69, '101', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(70, '102', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(71, '103', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(72, '104', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(73, '100', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(74, '101', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(75, '102', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(76, '103', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(77, '104', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(78, '100', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(79, '101', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(80, '102', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(81, '103', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(82, '104', 'test', './lib/picture/ddog.jpg', 0, '0000-00-00 00:00:00', NULL, 0, 0, 0),
(83, '100', 'test', './lib/picture/ddog.jpg', 0, '2014-09-08 08:20:33', 60, 1, 0, NULL),
(84, '101', 'test', './lib/picture/ddog.jpg', 0, '2014-09-08 08:20:33', 60, 1, 0, NULL),
(85, '102', 'test', './lib/picture/ddog.jpg', 0, '2014-09-08 08:20:33', 60, 1, 0, NULL),
(86, '103', 'test', './lib/picture/ddog.jpg', 0, '2014-09-08 08:20:33', 60, 1, 0, NULL),
(87, '104', 'test', './lib/picture/ddog.jpg', 0, '2014-09-08 08:20:33', 60, 1, 1, 1),
(88, '34567', '98765', './lib/picture/ddog.jpg', 500, '2014-09-08 12:59:59', 60, 1, 1, NULL),
(89, '34567', '98765', './lib/picture/ddog.jpg', 500, '2014-09-08 13:01:23', 60, 1, 1, NULL),
(90, '34567', '98765', './lib/picture/ddog.jpg', 500, '2014-09-08 13:01:28', 60, 1, 1, NULL),
(91, '汪汪', '汪汪汪汪汪汪', './lib/picture/ddog.jpg', 500, '2014-09-08 13:01:28', 60, 1, 1, 1);

-- --------------------------------------------------------

--
-- 資料表結構 `messageboard`
--

CREATE TABLE IF NOT EXISTS `messageboard` (
`id` int(11) NOT NULL,
  `itemid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `message` varchar(300) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=41 ;

--
-- 資料表的匯出資料 `messageboard`
--

INSERT INTO `messageboard` (`id`, `itemid`, `userid`, `message`, `datetime`) VALUES
(16, 83, 1, 'I love you.', '2014-08-28 08:22:22'),
(17, 83, 1, 'I love you.', '2014-08-28 08:29:06'),
(18, 83, 1, 'I love you.', '2014-08-29 02:02:50'),
(36, 91, 1, 'testestestest', '2014-09-08 13:23:11'),
(37, 91, 1, 'PhpUnitTest', '2014-09-08 21:09:03'),
(38, 91, 1, 'PhpUnitTest', '2014-09-08 21:09:12'),
(39, 91, 1, 'asdfasdfasdf', '2014-09-08 21:19:25'),
(40, 91, 1, '許功蓋', '2014-09-08 21:19:48');

-- --------------------------------------------------------

--
-- 資料表結構 `order`
--

CREATE TABLE IF NOT EXISTS `order` (
`id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `itemid` int(11) NOT NULL,
  `currentprice` int(11) NOT NULL,
  `datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 資料表結構 `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL DEFAULT '1234'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=66 ;

--
-- 資料表的匯出資料 `user`
--

INSERT INTO `user` (`id`, `username`, `password`) VALUES
(1, 'garth', '1234'),
(2, 'louis', '1234'),
(3, 'peter', '1234');

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `item`
--
ALTER TABLE `item`
 ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `messageboard`
--
ALTER TABLE `messageboard`
 ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `order`
--
ALTER TABLE `order`
 ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `item`
--
ALTER TABLE `item`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=92;
--
-- 使用資料表 AUTO_INCREMENT `messageboard`
--
ALTER TABLE `messageboard`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- 使用資料表 AUTO_INCREMENT `order`
--
ALTER TABLE `order`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用資料表 AUTO_INCREMENT `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=66;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
