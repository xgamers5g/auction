<?php
namespace AuctionTest\Model;

use PHPUnit_Framework_TestCase;
use Auction\Model\ItemModel;
use Auction\Model\ItemListModel;
use Fruit\Seed;
use Fruit\Config;
use DateTime;
use PDO;

//ini_set("display_errors", "On");
//error_reporting(E_ALL&~E_NOTICE);
define('BASE_DIR', dirname(__DIR__));
require_once("../../vendor/autoload.php");
Seed::Fertilize(new Config(BASE_DIR));

class ItemModelTest extends PHPUnit_Framework_TestCase
{
    private $im = null;

    public function testgetItemById()
    {
        $this->im = ItemModel::load(91);
        $fakeres = array(
                'id' => 91,
                'title' => '汪汪汪',
                'detail' => '汪汪汪汪汪汪',
                'image' => './lib/picture/ddog.jpg',
                'currentprice' => 500,
                'startdatetime' => '2014-09-08 21:01:28',
                'stopdatetime' => '2014-09-08 21:02:28',
                'available' => 1,
                'username' => 'garth',
                'bidusername' => 'garth'
        );
        $this->assertEquals($fakeres, $this->im->getItemById(), "getItemById物件有問題");
        unset($fakeres);
    }
    public function testgetMessageByItemId()
    {
        $this->im = ItemModel::load(83);
        $fakeres = array(
            0 => array(
                'id' => 16,
                'username' => 'garth',
                'message' => 'I love you.',
                'datetime' => '2014-08-28 16:22:22'
                ),
            1 => array(
                'id' => 17,
                'username' => 'garth',
                'message' => 'I love you.',
                'datetime' => '2014-08-28 16:29:06'
                ),
            2 => array(
                'id' => 18,
                'username' => 'garth',
                'message' => 'I love you.',
                'datetime' => '2014-08-29 10:02:50'
                )
        );
        var_dump($this->im->getMessageByItemId());
        $this->assertEquals($fakeres, $this->im->getMessageByItemId(), "getMessageByItemId物件有問題");
        unset($fakeres);
    }
    public function testaddMessage()
    {
        $datetime = new DateTime();
        $this->im = ItemModel::load(91);
        ItemModel::addMessage(91, 1, 'PhpUnitTest');
        $fakeres = array(
            0 => array(
                'id' => 36,
                'username' => 'garth',
                'message' => 'testtesttest',
                'datetime' => '2014-09-08 21:23:11'
                ),
            1 => array(
                'id' => 37,
                'username' => 'garth',
                'message' => 'PhpUnitTest',
                'datetime' => $datetime->format('Y-m-d H:i:s')
                )
            );
        $this->assertEquals($fakeres, $this->im->getMessageByItemId(), "addMessage方法有問題");
    }
    /*
    public function testCreate()
    {
        $constr= "mysql:dbname=auction;host=localhost;charset=utf8";
        $pdo = new PDO($constr, 'root', '1234');
        $this->im = ItemModel::create(
            '34567',
            '98765',
            './lib/picture/ddog.jpg',
            '500',
            '60',
            '1'
        );
        $this->testid = ($this->im->getConnect()->lastInsertId());
        //echo $this->testid;
        $this->assertEquals($pdo, $this->im->getConnect(), "資料連線有問題");
    }
    */

}
