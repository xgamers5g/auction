<?php
namespace AuctionTest\Model;

use PHPUnit_Framework_TestCase;
use Auction\Model\ItemModel;
use Auction\Model\ItemListModel;
use Fruit\Seed;
use Fruit\Config;
use PDO;

//ini_set("display_errors", "On");
//error_reporting(E_ALL&~E_NOTICE);
define('BASE_DIR', dirname(__DIR__));
require_once("../../vendor/autoload.php");
Seed::Fertilize(new Config(BASE_DIR));

class ItemListModelTest extends PHPUnit_Framework_TestCase
{
    private $ilm = null;
    private $im = null;
    public function testload()
    {    
        $constr= "mysql:dbname=auction;host=localhost;charset=utf8";
        $pdo = new PDO($constr, 'root', '1234');
        $this->ilm = ItemListModel::load();
        $this->assertEquals(
            $pdo,
            $this->ilm->getConnect(),
            "資料連線有問題"
        );
    }
    public function testgetItemList()
    {
        $this->ilm = ItemListModel::load();
        for ($i=100;$i<105;$i++) {
            ItemModel::create($i, 'test', './lib/picture/ddog.jpg', 300);
        }
        $test = array(
            0 => array('title'=>104),
            1 => array('title'=>103),
            2 => array('title'=>102),
            3 => array('title'=>101),
            4 => array('title'=>100)
            );
        //echo 123;
        //var_dump($this->ilm->getItemList());
        $this->assertEquals($test, $this->ilm->getItemList(), '不對喔');
        $lastid = $this->ilm->getConnect()->lastInsertId();
        $this->im = ItemModel::load($lastid);
        for($i=$lastid;$i>($lastid-5);$i--)
        {
            $this->im->deleteItem();
            $this->im->setId($i-1);
        }
    }
}