<?php return Pux\Mux::__set_state(array(
   'routes' => 
  array (
    0 => 
    array (
      0 => true,
      1 => '#^    /item/edit
    /(?P<iid>\\d+)
$#xs',
      2 => 
      array (
        0 => 'Auction\\Controller\\ItemController',
        1 => 'editItem',
      ),
      3 => 
      array (
        'require' => 
        array (
          'iid' => '\\d+',
        ),
        'variables' => 
        array (
          0 => 'iid',
        ),
        'regex' => '    /item/edit
    /(?P<iid>\\d+)
',
        'tokens' => 
        array (
          0 => 
          array (
            0 => 3,
            1 => '/item/edit',
          ),
          1 => 
          array (
            0 => 2,
            1 => '/',
            2 => '\\d+',
            3 => 'iid',
          ),
        ),
        'compiled' => '#^    /item/edit
    /(?P<iid>\\d+)
$#xs',
        'pattern' => '/item/edit/:iid',
      ),
    ),
    1 => 
    array (
      0 => true,
      1 => '#^    /item
    /(?P<iid>\\d+)
$#xs',
      2 => 
      array (
        0 => 'Auction\\Controller\\ItemController',
        1 => 'index',
      ),
      3 => 
      array (
        'require' => 
        array (
          'iid' => '\\d+',
        ),
        'method' => 1,
        'variables' => 
        array (
          0 => 'iid',
        ),
        'regex' => '    /item
    /(?P<iid>\\d+)
',
        'tokens' => 
        array (
          0 => 
          array (
            0 => 3,
            1 => '/item',
          ),
          1 => 
          array (
            0 => 2,
            1 => '/',
            2 => '\\d+',
            3 => 'iid',
          ),
        ),
        'compiled' => '#^    /item
    /(?P<iid>\\d+)
$#xs',
        'pattern' => '/item/:iid',
      ),
    ),
    2 => 
    array (
      0 => false,
      1 => '/item/addorder',
      2 => 
      array (
        0 => 'Auction\\Controller\\ItemController',
        1 => 'addOrder',
      ),
      3 => 
      array (
        'method' => 2,
      ),
    ),
    3 => 
    array (
      0 => false,
      1 => '/login/check',
      2 => 
      array (
        0 => 'Auction\\Controller\\LoginController',
        1 => 'checkUser',
      ),
      3 => 
      array (
        'method' => 2,
      ),
    ),
    4 => 
    array (
      0 => false,
      1 => '/item/addmsg',
      2 => 
      array (
        0 => 'Auction\\Controller\\ItemController',
        1 => 'addMessage',
      ),
      3 => 
      array (
        'method' => 2,
      ),
    ),
    5 => 
    array (
      0 => false,
      1 => '/user/edit',
      2 => 
      array (
        0 => 'Auction\\Controller\\UserController',
        1 => 'editUser',
      ),
      3 => 
      array (
      ),
    ),
    6 => 
    array (
      0 => false,
      1 => '/user/edit',
      2 => 
      array (
        0 => 'Auction\\Controller\\UserController',
        1 => 'updateUser',
      ),
      3 => 
      array (
        'method' => 2,
      ),
    ),
    7 => 
    array (
      0 => false,
      1 => '/item/edit',
      2 => 
      array (
        0 => 'Auction\\Controller\\ItemController',
        1 => 'updateItem',
      ),
      3 => 
      array (
        'method' => 2,
      ),
    ),
    8 => 
    array (
      0 => false,
      1 => '/itemlist',
      2 => 
      array (
        0 => 'Auction\\Controller\\ItemListController',
        1 => 'index',
      ),
      3 => 
      array (
      ),
    ),
    9 => 
    array (
      0 => false,
      1 => '/user/add',
      2 => 
      array (
        0 => 'Auction\\Controller\\UserController',
        1 => 'addUser',
      ),
      3 => 
      array (
        'method' => 2,
      ),
    ),
    10 => 
    array (
      0 => false,
      1 => '/regist',
      2 => 
      array (
        0 => 'Auction\\Controller\\RegistController',
        1 => 'index',
      ),
      3 => 
      array (
      ),
    ),
    11 => 
    array (
      0 => false,
      1 => '/state',
      2 => 
      array (
        0 => 'Auction\\Controller\\StateController',
        1 => 'index',
      ),
      3 => 
      array (
      ),
    ),
    12 => 
    array (
      0 => false,
      1 => '/login',
      2 => 
      array (
        0 => 'Auction\\Controller\\LoginController',
        1 => 'index',
      ),
      3 => 
      array (
      ),
    ),
    13 => 
    array (
      0 => false,
      1 => '/user',
      2 => 
      array (
        0 => 'Auction\\Controller\\UserController',
        1 => 'index',
      ),
      3 => 
      array (
      ),
    ),
    14 => 
    array (
      0 => false,
      1 => '/',
      2 => 
      array (
        0 => 'Auction\\Controller\\MainController',
        1 => 'index',
      ),
      3 => 
      array (
      ),
    ),
  ),
   'staticRoutes' => 
  array (
  ),
   'routesById' => 
  array (
  ),
   'submux' => 
  array (
  ),
   'id' => NULL,
   'expand' => true,
)); /* version */