<?php
/**
*這是pux路由器的原始檔
*/
require(implode(DIRECTORY_SEPARATOR, [dirname(__DIR__), 'vendor', 'autoload.php']));

use Pux\Mux;

$mux = new Pux\Mux;
//MainController
//從這裡開始每一支程式都會去判斷這世界上
//是否有$_SESSION('user')以及$_SESSION('page')
//前端進入點
$mux->add('/', ['Auction\Controller\MainController', 'index']);
//開使排組件
//state頁裡的組件
//顯示實體
$mux->add('/state', ['Auction\Controller\StateController','index']);
//login頁裡的組件
//可見實體
$mux->add('/login', ['Auction\Controller\LoginController','index']);
//抽像行為
$mux->post('/login/check', ['Auction\Controller\LoginController','checkUser']);
//regist頁裡的組件
//可見實體
$mux->add('/regist', ['Auction\Controller\RegistController','index']);
//抽像行為
$mux->POST('/user/add', ['Auction\Controller\UserController','addUser']);
//itemlist頁裡的組件
//可見實體
$mux->add('/itemlist', ['Auction\Controller\ItemListController','index']);
//item頁裡的組件
//可見實體
$mux->get(
    '/item/:iid',
    ['Auction\Controller\ItemController', 'index'],
    ['require' => ['iid' => '\d+']]
);
//抽像行為
$mux->post('/item/addorder', ['Auction\Controller\ItemController','addOrder']);
$mux->post('/item/addmsg', ['Auction\Controller\ItemController','addMessage']);
//user頁裡的組件
//可見實體
$mux->add('/user', ['Auction\Controller\UserController','index']);
//useredit頁裡的組件
//可見實體
$mux->add('/user/edit', ['Auction\Controller\UserController','editUser']);
//抽像行為
$mux->post('/user/edit', ['Auction\Controller\UserController','updateUser']);
//itemedit頁裡的組件
//可見實體
$mux->add(
    '/item/edit/:iid',
    ['Auction\Controller\ItemController','editItem'],
    ['require' => ['iid' => '\d+']]
);
//抽像行為
$mux->post('/item/edit', ['Auction\Controller\ItemController','updateItem']);
return $mux;
