<?php

namespace Auction\System;

use Fruit\Session\PhpSession;
use Fruit\Seed;

class Controller extends Seed
{
    protected static $user = null;//當前使用者
    protected static $page = null;//當前所在頁面
    protected static $data = null;//當前接到的post data
    protected static $session = null;//當前所使用的session物件
    protected static $twig = null;

    public static function init()
    {
        if (self::$session == null) {
            self::$session = new PhpSession();
        }
        if (file_get_contents("php://input")!=null) {
            self::$data = json_decode(file_get_contents("php://input"), true);
        } else {
            self::$data = null;
        }
        if (self::$session->has('user')) {
            self::$user = self::$session->get('user');
        } else {
            self::$user = null;
        }
        if (self::$session->has('page')) {
            self::$page = self::$session->get('page');
        } else {
            self::$page = null;
        }
        if (self::$twig == null) {
            self::$twig = self::getConfig()->getTmpl();
        }
    }
}
