<?php
namespace Auction\System;

use Fruit\Seed;
use PDO;

class Model extends Seed
{
    protected static $db = null;
    protected static function init()
    {
        if (self::$db == null) {
            self::$db = self::getConfig()->getDb();
            self::$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        }
    }
}
