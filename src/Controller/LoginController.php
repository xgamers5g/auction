<?php
namespace Auction\Controller;

use Auction\System\Controller;
use Auction\Model\UserModel;

class LoginController extends Controller
{
    public function __construct()
    {
        self::init();
    }
    public function index()
    {
        echo json_encode(
            self::$twig->render('login.html'),
            JSON_UNESCAPED_UNICODE
        );
    }
    public function checkUser()
    {
        if (self::$data == null) {
            echo json_encode('賣歐北來拉', JSON_UNESCAPED_UNICODE);
        } else {
            $username = self::$data[0]['value'];
            $password = self::$data[1]['value'];
            if (UserModel::checkUser($username, $password)==true) {
                self::$session->set('user', self::$data[0]['value']);
                echo json_encode($username);
            }
        }
    }
}
