<?php
namespace Auction\Controller;

use Auction\System\Controller;
use Auction\Model\ItemListModel;

class ItemListController extends Controller
{
    private $ilm = null;
    public function __construct()
    {
        self::init();
        $this->ilm = ItemListModel::load();
    }
    //叫出頁面
    public function index()
    {
        self::$session->set('page', 'itemlist');
        $itemlist = $this->ilm->getItemList();
        echo json_encode(
            self::$twig->render('itemlist.html', array('itemlist' => $itemlist)),
            JSON_UNESCAPED_UNICODE
        );
    }
}
