<?php
namespace Auction\Controller;

use Auction\System\Controller;

class MainController extends Controller
{
    public function __construct()
    {
        self::init();
    }
    public function index()
    {
        echo self::$twig->render('main.html');
    }
}
