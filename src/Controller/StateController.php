<?php
namespace Auction\Controller;

use Auction\System\Controller;

class StateController extends Controller
{
    public function __construct()
    {
        self::init();
    }
    public function index()
    {
        echo json_encode(
            self::$twig->render('state.html'),
            JSON_UNESCAPED_UNICODE
        );
    }
}
