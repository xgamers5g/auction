<?php
namespace Auction\Controller;

use Auction\System\Controller;
use Auction\Model\ItemModel;
use Auction\Model\MbModel;
use Auction\Model\OrderModel;
use Auction\Model\UserModel;

class ItemController extends Controller
{
    private $im = null;
    public function __construct()
    {
        self::init();
    }
    /**
     *  裡是商品物件的用戶端主頁
     */
    public function index($iid)
    {
        //self::$session->set('page', 'item');
        $this->iid = $iid;
        $this->im = ItemModel::load($this->iid);
        $item = $this->im->getItemById();
        $msgs = $this->im->getMessageByItemId();
        $uid = UserModel::getUserIdByUserName(self::$user);
        echo json_encode(
            self::$twig->render(
                'item.html',
                array(
                    'item' => $item,
                    'msgs' => $msgs,
                    'user' => self::$user,
                    'uid' => $uid
                    )
            ),
            JSON_UNESCAPED_UNICODE
        );
    }
    public function addMessage()
    {
        if (self::$data == null) {
            echo json_encode('賣歐北來拉', JSON_UNESCAPED_UNICODE);
        } else {
            $iid = self::$data[0]['value'];
            $uid = self::$data[1]['value'];
            $message = self::$data[2]['value'];
            ItemModel::addMessage($iid, $uid, $message);
            $this->im = ItemModel::load($iid);
            $item = $this->im->getItemById();
            $msgs = $this->im->getMessageByItemId();
            $uid = UserModel::getUserIdByUserName(self::$user);
            echo json_encode(
                self::$twig->render(
                    'item.html',
                    array(
                        'item' => $item,
                        'msgs' => $msgs,
                        'user' => self::$user,
                        'uid' => $uid
                    )
                ),
                JSON_UNESCAPED_UNICODE
            );
        }
    }
    /*
    public function addOrder()
    {
    }
    */
}
