<?php
namespace Auction\Model;

use Auction\System\Model;
use PDO;

class UserModel extends Model
{
    private static $id = null;
    protected function __construct($id)
    {
        self::$id = $id;
    }
    /**
     *static 登入檢查
     */
    public static function checkUser($username, $password)
    {
        self::init();
        $qs ="select password from user where username = ?";
        //echo $qs;
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $username, PDO::PARAM_STR);
        if ($stmt->execute()) {
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            //echo $result['password'];
            if ($result['password']==$password) {
                return true;
            }
        }
        $stmt->closeCursor();
        unset($result);
    }
    /**
     *給name拿id
     */
    public static function getUserIdByUserName($username)
    {
        self::init();
        $qs ="select id from user where username = ?";
        //echo $qs;
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $username, PDO::PARAM_STR);
        if ($stmt->execute()) {
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            return $result['id'];
        }
        $stmt->closeCursor();
        unset($result);
    }
    /**
     * 新增一個使用者，並讀取該使用者物件
     * @param username $u user username
     * @return UserModel object, or null
     */
    /*
    public static function create($u)
    {
        $um = null;
        self::init();
        $qs = "insert into user (username) values(?)";
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $u, PDO::PARAM_STR);
        if ($stmt->execute()) {
            $um = new self(self::$db->lastInsertId());
        }
        $stmt->closeCursor();
        return $um;
    }
    */
    /**
     * 讀取一個使用者物件
     * @return UserModel object, or null
     */
    /*
    public static function load($id)
    {
        self::init();
        return new self($id);
    }
    */
}
