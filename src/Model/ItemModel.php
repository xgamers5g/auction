<?php
namespace Auction\Model;

use Auction\System\Model;
use PDO;

class ItemModel extends Model
{
    //private static $db = null;  //extends Model
    private static $id = null;
    protected function __construct($id)
    {
        self::$id = $id;
    }
    /**
     * 新增一個商品，並讀取該商品物件
     * @param Title $t item title
     * @param Detail $d item detail
     * @param image $i item image
     * @param currentprice $o item origin price
     * @param maturity $m maturity second
     * @param userid $u userid
     * @return ItemModel object, or null
     */
    public static function create($t, $d, $i, $c, $m, $u)
    {
        $im = null;
        self::init();
        $qs = "insert into item (title, detail, image, currentprice, maturity, userid)
        values(?, ?, ?, ?, ?, ?)";
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $t, PDO::PARAM_STR);
        $stmt->bindValue(2, $d, PDO::PARAM_STR);
        $stmt->bindValue(3, $i, PDO::PARAM_STR);
        $stmt->bindValue(4, $c, PDO::PARAM_INT);
        $stmt->bindValue(5, $m, PDO::PARAM_INT);
        $stmt->bindValue(6, $u, PDO::PARAM_INT);
        if ($stmt->execute()) {
            $im = new self(self::$db->lastInsertId());
        }
        $stmt->closeCursor();
        return $im;
    }
    /**
     * 讀取一個商品物件 
     * @return ItemModel object, or null
     */
    public static function load($id)
    {
        self::init();
        return new self($id);
    }
    /**
     * 依照當前物件的ID來取回一筆商品資訊
     */
    public function getItemById()
    {
        $result = null;
        $qs = "
            select item.id as id,
            item.title as title,
            item.detail as detail,
            item.image as image, 
            item.currentprice as currentprice,
            item.startdatetime as startdatetime,
            DATE_ADD(item.startdatetime, INTERVAL item.maturity SECOND) as stopdatetime,
            item.available as available,
            user1.username as username,
            user2.username as bidusername
            from item join user user1 on item.userid = user1.id
            join user user2 on item.biduserid = user2.id 
            where item.id = ?";
        //echo $qs;
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, self::$id, PDO::PARAM_INT);
        if ($stmt->execute()) {
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
        }
        $stmt->closeCursor();
        return $result;
    }
    //*
    //取出所有此商品下的留言
    //*
    public function getMessageByItemId()
    {
        $result = null;
        $qs = "
            select messageboard.id as id,
            user.username as username,
            messageboard.message as message,
            messageboard.datetime as datetime
            from messageboard join user on messageboard.userid = user.id
            where messageboard.itemid = ?";
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, self::$id, PDO::PARAM_INT);
        if ($stmt->execute()) {
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        $stmt->closeCursor();
        return $result;
    }
    /**
     * 依照當前物件的ID來增加留言
     * @param iid $i item id
     * @param uid $u user id
     * @param message $m messageboard message
     */
    public static function addMessage($i, $u, $m)
    {
        self::init();
        $qs = "insert into messageboard (itemid, userid, message) values(?, ?, ?)";
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $i, PDO::PARAM_INT);
        $stmt->bindValue(2, $u, PDO::PARAM_INT);
        $stmt->bindValue(3, $m, PDO::PARAM_STR);
        if ($stmt->execute()) {
            $result = true;
        }
        $result = false;
        $stmt->closeCursor();
        return $result;
    }
    /**
     * 對此物品下單
     */
    public function addOrder()
    {

    }
    /**
     * 依照當前物件的ID來更新一筆商品資訊
     */
    /*
    public function updateItem($t, $d, $i, $o)
    {
        $result = null;
        $qs = "update item set title=?, detail=?, image=?,originprice=? where id = ?";
        //echo $qs;
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $t, PDO::PARAM_STR);
        $stmt->bindValue(2, $d, PDO::PARAM_STR);
        $stmt->bindValue(3, $i, PDO::PARAM_STR);
        $stmt->bindValue(4, $o, PDO::PARAM_INT);
        $stmt->bindValue(5, self::$id, PDO::PARAM_INT);
        if ($stmt->execute()) {
            $result = true;
        } else {
            $result = false;
        }
        $stmt->closeCursor();
        return $result;
    }
    */
    /**
     * 依照當前物件的ID來刪除一筆商品資訊
     */
    /*
    public function deleteItem()
    {
        $result = null;
        $qs = "delete from item where id = ?";
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, self::$id, PDO::PARAM_INT);
        if ($stmt->execute()) {
            $result = true;
        } else {
            $result = false;
        }
        $stmt->closeCursor();
        return $result;
    }
    */
    /**
     * 取得當前物件正在運作的資料庫連線物件
     */
    public function getConnect()
    {
        self::init();
        return self::$db;
    }
    /**
     * 更改id
     */
    public function setId($id)
    {
        self::$id = $id;
    }
}
