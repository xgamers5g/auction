<?php
namespace Auction\Model;

use Auction\System\Model;
use PDO;

class ItemListModel extends Model
{
    public static function load()
    {
        self::init();
        return new self();
    }
    public function getItemList()
    {
        $result = null;
        $qs ="select id,title from item order by id desc";
        $stmt = self::$db->prepare($qs);
        if ($stmt->execute()) {
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        $stmt->closeCursor();
        return $result;
    }
    public function getConnect()
    {
        self::init();
        return self::$db;
    }
}
